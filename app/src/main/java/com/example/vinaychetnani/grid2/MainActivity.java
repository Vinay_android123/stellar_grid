package com.example.vinaychetnani.grid2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String type;
    private TabLayout tabLayout, tabLayout1;
    private ViewPager viewPager, viewPager1;
    private Context mContext;
    public int var_j = 1;public int var_k = 1;public int var_l = 1;public int var_m = 1;public int var_n = 1;public int var_o = 1;public int var_p = 1;
    public int roofarea_var = 1;
    public int electricityrate_var = 1;
    FragmentManager fm = getSupportFragmentManager();
    private Button mButton;
    ScrollView myView;
    EditText monthlybill,roofarea,electricityrate;
    LinearLayout mRelativeLayout,l1,l2,l3,l4;
    TextView tva,tvb,tvc,tvd,e,f,g,h,i,j,k,l,m,n,o,p,e1,e2,e3,e4;
    Spinner spi;
    CardView solarpowercard;
    CardView economicscard;
    CardView basicscard;
    CardView netmeteringcard;
    CardView tab;
    CardView tab1;
    CardView tab23;
    CardView tab231;
    CardView tab2314;
    CardView tab23145;
    String bill1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI(findViewById(R.id.parent));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setIcon(R.drawable.logo1);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager1 = (ViewPager) findViewById(R.id.viewpager1);
        setupViewPager1(viewPager1);

        animatemain();
        animatesolarpower();
        animateeconomics();
        animatebasics();
        animatenetmetering();
        animatenext();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        spi =(Spinner) findViewById(R.id.spinner1);
        spi.setBackgroundResource(R.drawable.my_button_bg1);

        e1 = new TextView(this);
        e2 = new TextView(this);
        e3 = new TextView(this);
        e4 = new TextView(this);

        roofarea=(EditText) findViewById(R.id.roofarea);
        roofarea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (roofarea.getTag() == null) roofarea_var = 2;
            }
        });
        electricityrate =(EditText) findViewById(R.id.electricityrate);
        electricityrate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (electricityrate.getTag() == null) electricityrate_var = 2;
            }
        });
        monthlybill =(EditText) findViewById(R.id.monthlybill);
        monthlybill.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bill1 = monthlybill.getText().toString().trim();
                if (bill1 == null) {
                    Log.d("fsd", "hi");
                } else if ("".equals(bill1)) {
                    if (roofarea_var == 1) {
                        roofarea.setTag("hello");
                        roofarea.setText("");
                        roofarea.setTag(null);
                    }
                    if (electricityrate_var == 1) {
                        electricityrate.setTag("there");
                        electricityrate.setText("");
                        electricityrate.setTag(null);
                    }

                } else {
                    int z = Integer.parseInt(bill1) * 2;
                    if (roofarea_var == 1) {
                        roofarea.setTag("Hello");
                        roofarea.setText(Integer.toString(z));
                        roofarea.setTag(null);
                    }
                    if (electricityrate_var == 1) {
                        electricityrate.setTag("there");
                        electricityrate.setText(Integer.toString(z * 8));
                        electricityrate.setTag(null);
                    }
                    tva.setText(Integer.toString(z * 2));
                    e.setText(Integer.toString(z * 1));
                    i.setText(Integer.toString(z * 3));
                    m.setText(Integer.toString(z * 2));
                }
            }
        });

        spi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int h, long l) {
                type = Integer.toString(spi.getSelectedItemPosition() + 1);
//                Toast.makeText(MainActivity.this,
//                        "On Button Click : " +
//                                "\n" + type ,
//                        Toast.LENGTH_SHORT).show();
                final String sp = spi.getSelectedItem().toString().trim();
                if (sp.equals("NDS")) {
                    String bill = monthlybill.getText().toString().trim();
                    if (bill1 == null) {
                        Log.d("fsd", "hi");
                    } else if ("".equals(bill1)) {
                        if (roofarea_var == 1) {
                            roofarea.setTag("hello");
                            roofarea.setText("");
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText("");
                            electricityrate.setTag(null);
                        }
                    } else {
                        int z = Integer.parseInt(bill) * 2;
                        if (roofarea_var == 1) {
                            roofarea.setTag("Hello");
                            roofarea.setText(Integer.toString(z));
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText(Integer.toString(z * 8));
                            electricityrate.setTag(null);
                        }
                    }
                } else if (sp.equals("DOMESTIC")) {
                    bill1 = monthlybill.getText().toString().trim();
                    if (bill1 == null) {
                        Log.d("fsd", "hi");
                    } else if ("".equals(bill1)) {
                        if (roofarea_var == 1) {
                            roofarea.setTag("hello");
                            roofarea.setText("");
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText("");
                            electricityrate.setTag(null);
                        }
                    }else{
                        int z = Integer.parseInt(bill1);
                        if (roofarea_var == 1) {
                            roofarea.setTag("Hello");
                            roofarea.setText(Integer.toString(z));
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText(Integer.toString(z * 8));
                            electricityrate.setTag(null);
                        }
                    }
                } else {
                    bill1 = monthlybill.getText().toString().trim();
                    if (bill1 == null) {
                        Log.d("fsd", "hi");
                    } else if ("".equals(bill1)) {
                        if (roofarea_var == 1) {
                            roofarea.setTag("hello");
                            roofarea.setText("");
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText("");
                            electricityrate.setTag(null);
                        }
                    }else{
                        int z = Integer.parseInt(bill1) * 3;
                        if (roofarea_var == 1) {
                            roofarea.setTag("Hello");
                            roofarea.setText(Integer.toString(z));
                            roofarea.setTag(null);
                        }
                        if (electricityrate_var == 1) {
                            electricityrate.setTag("there");
                            electricityrate.setText(Integer.toString(z * 8));
                            electricityrate.setTag(null);
                        }
                    }
                }
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                type = "0";
                return;
            }
        });

        tabLayout1 = (TabLayout) findViewById(R.id.tabs1);
        tabLayout1.setupWithViewPager(viewPager1);

        mContext = getApplicationContext();

        mRelativeLayout = (LinearLayout) findViewById(R.id.rl);
        mButton = (Button) findViewById(R.id.calculatebtn);

        myView = (ScrollView) findViewById(R.id.scroll);
        myView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                if (solarpowercard.hasFocus() && (var_j == 1)) {
                    animatesolarpower();
                    var_j+=1;
                } else if (economicscard.hasFocus() && (var_k == 1)) {
                    animateeconomics();
                    var_k+=1;
                } else if (basicscard.hasFocus() && (var_l == 1)) {
                    animatebasics();
                    var_l+=1;
                } else if (netmeteringcard.hasFocus() && (var_m == 1)) {
                    animatenetmetering();
                    var_m+=1;
                } else if (tab.hasFocus() && (var_n == 1)) {
                    animateprices();
                    var_n+=1;
                } else if (tab1.hasFocus() && (var_o == 1)) {
                    animateprices();
                    var_o+=1;
                } else if (tab231.hasFocus() && (var_p == 1)) {
                    animateprices();
                    var_p+=1;
                }
                //Log.d("hello","hi");
            }
        });

        solarpowercard = (CardView) findViewById(R.id.solarpowercard);
        economicscard = (CardView) findViewById(R.id.economicscard);
        basicscard = (CardView) findViewById(R.id.basicscard);
        netmeteringcard = (CardView) findViewById(R.id.netmeteringcard);
        tab = (CardView) findViewById(R.id.tab);
        tab1 = (CardView) findViewById(R.id.tab1);
        tab23 = (CardView) findViewById(R.id.tab23);
        tab231 = (CardView) findViewById(R.id.tab231);
        tab2314 = (CardView) findViewById(R.id.tab2314);
        tab23145 = (CardView) findViewById(R.id.tab23145);

        final CardView card = new CardView(mContext);
        final CardView card1 = new CardView(mContext);
        final CardView card2 = new CardView(mContext);
        final CardView card3 = new CardView(mContext);
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );

        final LinearLayout.LayoutParams line =new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                5
        );
        line.setMargins(125,15,125,15);

        final LinearLayout.LayoutParams paramsnum = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
        );

        paramsnum.setMargins(5, 5, 5, 5);
        params.topMargin=15;
        params.bottomMargin=15;
        params.leftMargin=15;
        params.rightMargin=15;

        l1= new LinearLayout(this);
        l1.setOrientation(LinearLayout.VERTICAL);
        l1.setLayoutParams(paramsnum);

        l2= new LinearLayout(this);
        l2.setOrientation(LinearLayout.VERTICAL);
        l2.setLayoutParams(params);

        l3= new LinearLayout(this);
        l3.setOrientation(LinearLayout.VERTICAL);
        l3.setLayoutParams(params);

        l4= new LinearLayout(this);
        l4.setOrientation(LinearLayout.VERTICAL);
        l4.setLayoutParams(params);

        tva = new TextView(mContext);
        tva.setLayoutParams(paramsnum);
        tvb = new TextView(mContext);
        tvb.setLayoutParams(params);
        tvc = new TextView(mContext);
        tvc.setLayoutParams(params);
        tvd = new TextView(mContext);
        tvd.setLayoutParams(params);

        e = new TextView(mContext);
        e.setLayoutParams(paramsnum);
        f = new TextView(mContext);
        f.setLayoutParams(params);
        g= new TextView(mContext);
        g.setLayoutParams(params);
        h= new TextView(mContext);
        h.setLayoutParams(params);

        i = new TextView(mContext);
        i.setLayoutParams(paramsnum);
        j = new TextView(mContext);
        j.setLayoutParams(params);
        k = new TextView(mContext);
        k.setLayoutParams(params);
        l= new TextView(mContext);
        l.setLayoutParams(params);

        m = new TextView(mContext);
        m.setLayoutParams(paramsnum);
        n = new TextView(mContext);
        n.setLayoutParams(params);
        o = new TextView(mContext);
        o.setLayoutParams(params);
        p = new TextView(mContext);
        p.setLayoutParams(params);

        final View v = new View(this);
        v.setLayoutParams(line);
        v.setBackgroundColor(Color.parseColor("#AE1B18"));

        final View v1 = new View(this);
        v1.setLayoutParams(line);
        v1.setBackgroundColor(Color.parseColor("#3D0C4A"));

        final View v2 = new View(this);
        v2.setLayoutParams(line);
        v2.setBackgroundColor(Color.parseColor("#093D30"));

        final View v3 = new View(this);
        v3.setLayoutParams(line);
        v3.setBackgroundColor(Color.parseColor("#5D1133"));

        card.addView(l1);
        card1.addView(l2);
        card2.addView(l3);
        card3.addView(l4);

        mRelativeLayout.addView(card);
        mRelativeLayout.addView(card1);
        mRelativeLayout.addView(card2);
        mRelativeLayout.addView(card3);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialize a new CardView
                card.setLayoutParams(params);
                card.setRadius(0);
                card.setCardBackgroundColor(Color.WHITE);
                card1.setCardBackgroundColor(Color.WHITE);
                card2.setCardBackgroundColor(Color.WHITE);
                card3.setCardBackgroundColor(Color.WHITE);
                card.setContentPadding(4, 20, 4, 30);
                card.setMaxCardElevation(15);
                card.setCardElevation(9);

                card1.setLayoutParams(params);
                card1.setRadius(0);
                card1.setContentPadding(4, 20, 4, 30);
                card1.setMaxCardElevation(15);
                card1.setCardElevation(9);

                card2.setLayoutParams(params);
                card2.setRadius(0);
                card2.setContentPadding(4, 20, 4, 30);
                card2.setMaxCardElevation(15);
                card2.setCardElevation(9);

                card3.setLayoutParams(params);
                card3.setRadius(0);
                card3.setContentPadding(4, 20, 4, 30);
                card3.setMaxCardElevation(15);
                card3.setCardElevation(9);

                tva.setTextSize(200);
                tva.setTextColor(Color.parseColor("#FF605F"));
                tva.setGravity(Gravity.CENTER);
                l1.removeAllViews();
                l1.addView(tva);

                tvb.setText("Years");
                tvb.setTextSize(25);
                tvb.setTextColor(Color.parseColor("#FF6968"));
                tvb.setGravity(Gravity.CENTER);
                l1.addView(tvb);

                tvc.setText("Recovery Period");
                tvc.setTextSize(25);
                tvc.setTextColor(Color.parseColor("#CF252C"));
                tvc.setGravity(Gravity.CENTER);
                l1.addView(tvc);

                l1.addView(v);
                tvd.setText("The time it takes for your system to deliver enough savings to cover your incurred upfront costs");
                tvd.setTextSize(12);
                tvd.setTextColor(Color.BLACK);
                tvd.setGravity(Gravity.CENTER);
                l1.addView(tvd);

                e.setTextSize(200);
                e.setTextColor(Color.parseColor("#9E46B8"));
                e.setGravity(Gravity.CENTER);
                l2.removeAllViews();
                l2.addView(e);

                f.setText("Kw");
                f.setTextSize(25);
                f.setTextColor(Color.parseColor("#AE66C4"));
                f.setGravity(Gravity.CENTER);
                l2.addView(f);

                g.setText("System Size");
                g.setTextSize(25);
                g.setTextColor(Color.parseColor("#56156D"));
                g.setGravity(Gravity.CENTER);
                l2.addView(g);
                l2.addView(v1);

                h.setText("The capacity of the System that is capable of generating the right amount of electricity as per your current consumption");
                h.setTextSize(12);
                h.setTextColor(Color.BLACK);
                h.setGravity(Gravity.CENTER);
                l2.addView(h);

                i.setTextSize(200);
                i.setTextColor(Color.parseColor("#2DCAA8"));
                i.setGravity(Gravity.CENTER);
                l3.removeAllViews();
                l3.addView(i);

                j.setText("Lacs");
                j.setTextSize(25);
                j.setTextColor(Color.parseColor("#7EDEC8"));
                j.setGravity(Gravity.CENTER);
                l3.addView(j);

                k.setText("System Cost");
                k.setTextSize(25);
                k.setTextColor(Color.parseColor("#218871"));
                k.setGravity(Gravity.CENTER);
                l3.addView(k);
                l3.addView(v2);

                l.setText("A close estimate of the price of the System that your custom Solar Power solution would cost.");
                l.setTextSize(12);
                l.setTextColor(Color.BLACK);
                l.setGravity(Gravity.CENTER);
                l3.addView(l);

                m.setTextSize(200);
                m.setTextColor(Color.parseColor("#E667A0"));
                m.setGravity(Gravity.CENTER);
                l4.removeAllViews();
                l4.addView(m);

                n.setText("Return");
                n.setTextSize(25);
                n.setTextColor(Color.parseColor("#EB83B1"));
                n.setGravity(Gravity.CENTER);
                l4.addView(n);

                o.setText("Total Investment Value");
                o.setTextSize(25);
                o.setTextColor(Color.parseColor("#A82962"));
                o.setGravity(Gravity.CENTER);
                l4.addView(o);
                l4.addView(v3);

                p.setText("The Total value of the investment received during the course of system’s operations");
                p.setTextSize(12);
                p.setTextColor(Color.BLACK);
                p.setGravity(Gravity.CENTER);
                l4.addView(p);

                mButton.setClickable(true);

                String bill = monthlybill.getText().toString().trim();
                String roofArea = roofarea.getText().toString().trim();
                if (roofarea.getTag()== null) roofArea = "";

                String rate = electricityrate.getText().toString().trim();
                if (electricityrate.getTag()== null) rate = "";
                String[] arr = calculator(bill, type, roofArea, rate);

                roofarea.setTag("heoo");
                roofarea.setText(arr[0]);
                roofarea.setTag(null);
                electricityrate.setTag("heoo");
                electricityrate.setText(arr[1]);
                electricityrate.setTag(null);
                tva.setText(arr[2]);
                e.setText(arr[3]);
                i.setText(arr[4]);
                m.setText(arr[5]);
            }
        });

        Button contactmebutton = (Button) findViewById(R.id.contactmebutton);
        contactmebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackFragment dialogFragment = new feedbackFragment();
                dialogFragment.show(fm, "feedbackFragment");
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {
            animatemain();
            startAnimateBackground();
            myView.smoothScrollTo(0, 0);
        }
        else if (id == R.id.about) {
            animateabout();
            myView.smoothScrollTo(0, tab.getTop());
            //new OneFragment().animateabout();
        }
        else if (id == R.id.features) {
            animatefeatures();
            myView.smoothScrollTo(0, tab1.getTop());
        }
        else if (id == R.id.prices) {
            animateprices();
            myView.smoothScrollTo(0, tab23.getTop());
        }
        else if (id == R.id.feedback) {
            animatefeedback();
            myView.smoothScrollTo(0, tab231.getTop());
        }
        else if (id == R.id.team) {
            myView.smoothScrollTo(0, tab2314.getTop());
        }
        else if (id == R.id.contacts) {
            myView.smoothScrollTo(0, tab23145.getTop());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startAnimateBackground (){
        RotateAnimation rotate
                = new RotateAnimation(0.0f, 1080.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        ScaleAnimation scale
                = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        AnimationSet set = new AnimationSet(true);
        set.addAnimation(rotate);
        set.addAnimation(scale);
        set.addAnimation(alpha);
        set.setDuration(1500);
        ImageView backgroundimage = (ImageView) findViewById(R.id.backgroundimage);
        backgroundimage.startAnimation(set);
    }

    private void animatemain(){
        TranslateAnimation moveToptoBottom = new TranslateAnimation(0,0, -1000, 0);
        moveToptoBottom.setDuration(250);
        moveToptoBottom.setFillAfter(true);
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        TextView maintitle = (TextView) findViewById(R.id.maintitle);
        maintitle.startAnimation(moveToptoBottom);
        TextView maintitle1 = (TextView) findViewById(R.id.maintitle1);
        maintitle1.startAnimation(moveLefttoRight);
        TextView mainsubtitle = (TextView) findViewById(R.id.mainsubtitle);
        mainsubtitle.startAnimation(moveRighttoLeft);
    }

    private void animateabout(){
        TranslateAnimation moveToptoBottom = new TranslateAnimation(0,0, -1000, 0);
        moveToptoBottom.setDuration(250);
        moveToptoBottom.setFillAfter(true);
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);

        TextView simplesteptitle = (TextView) findViewById(R.id.simplesteptitle);
        simplesteptitle.startAnimation(moveToptoBottom);
        TextView simplestepsubtitle = (TextView) findViewById(R.id.simplestepsubtitle);
        simplestepsubtitle.startAnimation(moveToptoBottom);
/*Fragment 1*/
        ImageView understandneedimg = (ImageView) findViewById(R.id.understandneedimg);
        understandneedimg.startAnimation(moveLefttoRight);
        TextView understandneedtitle = (TextView) findViewById(R.id.understandneedtitle);
        understandneedtitle.startAnimation(moveRighttoLeft);
        TextView understandneedsubtitle = (TextView) findViewById(R.id.understandneedsubtitle);
        understandneedsubtitle.startAnimation(moveRighttoLeft);

        ImageView buildsystemimg = (ImageView) findViewById(R.id.buildsystemimg);
        buildsystemimg.startAnimation(moveLefttoRight);
        TextView buildsystemtitle = (TextView) findViewById(R.id.buildsystemtitle);
        buildsystemtitle.startAnimation(moveRighttoLeft);
        TextView buildsystemsubtitle = (TextView) findViewById(R.id.buildsystemsubtitle);
        buildsystemsubtitle.startAnimation(moveRighttoLeft);

        ImageView beginsavingimg = (ImageView) findViewById(R.id.beginsavingimg);
        beginsavingimg.startAnimation(moveLefttoRight);
        TextView beginsavingtitle = (TextView) findViewById(R.id.beginsavingtitle);
        beginsavingtitle.startAnimation(moveRighttoLeft);
        TextView beginsavingsubtitle = (TextView) findViewById(R.id.beginsavingsubtitle);
        beginsavingsubtitle.startAnimation(moveRighttoLeft);
/*Fragment 2*/
        TextView incentivetitle = (TextView) findViewById(R.id.incentivetitle);
        incentivetitle.startAnimation(moveRighttoLeft);
        ImageView incentiveimg = (ImageView) findViewById(R.id.incentiveimg);
        incentiveimg.startAnimation(moveLefttoRight);
        TextView incentivesubtitle = (TextView) findViewById(R.id.incentivesubtitle);
        incentivesubtitle.startAnimation(moveRighttoLeft);
        TextView incentivesubtitlebelow = (TextView) findViewById(R.id.incentivesubtitlebelow);
        incentivesubtitlebelow.startAnimation(moveRighttoLeft);
    }

    private void animatefeatures(){
        TranslateAnimation moveToptoBottom = new TranslateAnimation(0,0, -1000, 0);
        moveToptoBottom.setDuration(250);
        moveToptoBottom.setFillAfter(true);
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        TextView advantagestitle = (TextView) findViewById(R.id.advantagestitle);
        advantagestitle.startAnimation(moveToptoBottom);
        TextView advantagessubtitle = (TextView) findViewById(R.id.advantagessubtitle);
        advantagessubtitle.startAnimation(moveToptoBottom);
/*Fragment A*/
        TextView intelligenttitle = (TextView) findViewById(R.id.intelligenttitle);
        if (intelligenttitle != null) intelligenttitle.startAnimation(moveRighttoLeft);
        ImageView intelligentimg = (ImageView) findViewById(R.id.intelligentimg);
        if (intelligentimg != null) intelligentimg.startAnimation(moveLefttoRight);
        TextView intelligentsubtitle = (TextView) findViewById(R.id.intelligentsubtitle);
        if (intelligentsubtitle != null) intelligentsubtitle.startAnimation(moveRighttoLeft);
/*Fragment B*/
        TextView customisedtitle = (TextView) findViewById(R.id.customisedtitle);
        customisedtitle.startAnimation(moveRighttoLeft);
        ImageView customisedimg = (ImageView) findViewById(R.id.customisedimg);
        customisedimg.startAnimation(moveLefttoRight);
        TextView customisedsubtitle = (TextView) findViewById(R.id.customisedsubtitle);
        customisedsubtitle.startAnimation(moveRighttoLeft);
/*Fragment C*/
        TextView easytitle = (TextView) findViewById(R.id.easytitle);
        if (easytitle != null) easytitle.startAnimation(moveRighttoLeft);
        ImageView easyimg = (ImageView) findViewById(R.id.easyimg);
        if (easyimg != null) easyimg.startAnimation(moveLefttoRight);
        TextView easysubtitle = (TextView) findViewById(R.id.easysubtitle);
        if (easysubtitle != null) easysubtitle.startAnimation(moveRighttoLeft);
    }

    private void animateprices(){
        TranslateAnimation moveToptoBottom = new TranslateAnimation(0,0, -1000, 0);
        moveToptoBottom.setDuration(950);
        moveToptoBottom.setFillAfter(true);
        TranslateAnimation moveBottomtoTop = new TranslateAnimation(0,0,1000,0);
        moveBottomtoTop.setDuration(950);
        moveBottomtoTop.setFillAfter(true);
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        TextView calculatetitle = (TextView) findViewById(R.id.calculatetitle);
        calculatetitle.startAnimation(moveToptoBottom);
        TextView calculatesubtitle = (TextView) findViewById(R.id.calculatesubtitle);
        calculatesubtitle.startAnimation(moveToptoBottom);
        EditText monthlybill = (EditText) findViewById(R.id.monthlybill);
        monthlybill.startAnimation(moveLefttoRight);
        Spinner connection = (Spinner) findViewById(R.id.spinner1);
        connection.startAnimation(moveRighttoLeft);
        EditText roofarea = (EditText) findViewById(R.id.roofarea);
        roofarea.startAnimation(moveLefttoRight);
        EditText electricityrate = (EditText) findViewById(R.id.electricityrate);
        electricityrate.startAnimation(moveRighttoLeft);
        Button calculatebtn = (Button) findViewById(R.id.calculatebtn);
        calculatebtn.startAnimation(moveBottomtoTop);
    }
    public void animatefeedback() {
        TranslateAnimation moveBottomtoTop = new TranslateAnimation(0,0,1000,0);
        moveBottomtoTop.setDuration(950);
        moveBottomtoTop.setFillAfter(true);
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        TextView nexttitle = (TextView) findViewById(R.id.nexttitle);
        nexttitle.startAnimation(moveLefttoRight);
        TextView nextsubtitle = (TextView) findViewById(R.id.nextsubtitle);
        nextsubtitle.startAnimation(moveRighttoLeft);
    }
    public void animatesolarpower() {
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        ImageView solarpowerimage = (ImageView) findViewById(R.id.solarpowerimage);
        solarpowerimage.setAnimation(moveRighttoLeft);
        TextView solarpowertitle = (TextView) findViewById(R.id.solarpowertitle);
        solarpowertitle.setAnimation(moveLefttoRight);
        TextView solarpowersubtitle = (TextView) findViewById(R.id.solarpowersubtitle);
        solarpowersubtitle.setAnimation(moveRighttoLeft);
        TextView solarpowersubtitlebelow = (TextView) findViewById(R.id.solarpowersubtitlebelow);
        solarpowersubtitlebelow.setAnimation(moveLefttoRight);
    }
    public void animateeconomics() {
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        ImageView economicsimage = (ImageView) findViewById(R.id.economicsimage);
        economicsimage.setAnimation(moveRighttoLeft);
        TextView economicstitle = (TextView) findViewById(R.id.economicstitle);
        economicstitle.setAnimation(moveLefttoRight);
        TextView economicssubtitle = (TextView) findViewById(R.id.economicssubtitle);
        economicssubtitle.setAnimation(moveRighttoLeft);
        TextView economicssubtitlebelow = (TextView) findViewById(R.id.economicssubtitlebelow);
        economicssubtitlebelow.setAnimation(moveLefttoRight);
    }
    public void animatebasics(){
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        ImageView basicsimage = (ImageView) findViewById(R.id.basicsimage);
        basicsimage.setAnimation(moveRighttoLeft);
        TextView basicstitle = (TextView) findViewById(R.id.basicstitle);
        basicstitle.setAnimation(moveLefttoRight);
        TextView basicssubtitle = (TextView) findViewById(R.id.basicssubtitle);
        basicssubtitle.setAnimation(moveRighttoLeft);
        TextView basicssubtitlebelow = (TextView) findViewById(R.id.basicssubtitlebelow);
        basicssubtitlebelow.setAnimation(moveLefttoRight);
    }
    public void animatenetmetering(){
        TranslateAnimation moveLefttoRight = new TranslateAnimation(-900,0, 0, 0);
        moveLefttoRight.setDuration(950);
        moveLefttoRight.setFillAfter(true);
        TranslateAnimation moveRighttoLeft = new TranslateAnimation(900,0, 0, 0);
        moveRighttoLeft.setDuration(950);
        moveRighttoLeft.setFillAfter(true);
        ImageView netmeteringimage = (ImageView) findViewById(R.id.netmeteringimage);
        netmeteringimage.setAnimation(moveRighttoLeft);
        TextView netmeteringtitle = (TextView) findViewById(R.id.netmeteringtitle);
        netmeteringtitle.setAnimation(moveLefttoRight);
        TextView netmeteringsubtitle = (TextView) findViewById(R.id.netmeteringsubtitle);
        netmeteringsubtitle.setAnimation(moveRighttoLeft);
        TextView netmeteringsubtitlebelow = (TextView) findViewById(R.id.netmeteringsubtitlebelow);
        netmeteringsubtitlebelow.setAnimation(moveLefttoRight);
    }
    public void animatenext(){
        TranslateAnimation moveToptoBottom = new TranslateAnimation(0,0, -1000, 0);
        moveToptoBottom.setDuration(250);
        moveToptoBottom.setFillAfter(true);
        TranslateAnimation moveBottomtoTop = new TranslateAnimation(0,0,1000,0);
        moveBottomtoTop.setDuration(500);
        moveBottomtoTop.setFillAfter(true);
        TextView nexttitle = (TextView) findViewById(R.id.nexttitle);
        nexttitle.setAnimation(moveToptoBottom);
        TextView nextsubtitle = (TextView) findViewById(R.id.nextsubtitle);
        nextsubtitle.setAnimation(moveBottomtoTop);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "HOW YOU CAN GO SOLAR");
        adapter.addFragment(new TwoFragment(), "WHY YOU SHOULD GO SOLAR");
        viewPager.setAdapter(adapter);
    }

    private void setupViewPager1(ViewPager viewPager1) {
        ViewPagerAdapter adapter1 = new ViewPagerAdapter(getSupportFragmentManager());
        adapter1.addFragment(new Fragmenta(), "I");
        adapter1.addFragment(new Fragment2(), "II");
        adapter1.addFragment(new Fragment3(), "III");
        viewPager1.setAdapter(adapter1);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    /* Perfect way to Hide Virtual keyboard. It is called in onCreate method with parent layout as its argument*/
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
    public static String[] calculator(String bill, String type, String roofArea, String rate) {
        String[] arr = new String[6];
        if ("".equals(bill)){
            bill = "0";
        }
        if ("".equals(roofArea)){
            roofArea = Integer.toString(Integer.parseInt(bill)*2);
        }
        if ("".equals(rate)) {
            rate = Integer.toString((Integer.parseInt(bill) < 215 && Integer.parseInt(type) == 1) ? 5 : 8);
        }
        int out0 = Integer.parseInt(bill)+Integer.parseInt(roofArea)+Integer.parseInt(rate);
        int out1 = Integer.parseInt(bill)-Integer.parseInt(roofArea)+Integer.parseInt(rate);
        int out2 = Integer.parseInt(bill)+Integer.parseInt(roofArea)-Integer.parseInt(rate);
        int out3 = Integer.parseInt(bill)-Integer.parseInt(roofArea)-Integer.parseInt(rate);
        arr[0] = (roofArea);
        arr[1] = (rate);
        arr[2] = Integer.toString(out0);
        arr[3] = Integer.toString(out1);
        arr[4] = Integer.toString(out2);
        arr[5] = Integer.toString(out3);
        return arr;
    }
}
