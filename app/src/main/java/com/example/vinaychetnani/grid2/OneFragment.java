package com.example.vinaychetnani.grid2;

/**
 * Created by Vinay Chetnani on 6/18/2016.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.example.vinaychetnani.grid2.R;


public class OneFragment extends Fragment{

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);
//        simplesteptitle = (TextView) view.findViewById(R.id.simplesteptitle);
//        simplestepsubtitle = (TextView) view.findViewById(R.id.simplestepsubtitle);
        return view;
    }

}